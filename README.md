# hugo-build

Public image with [hugo](https://gohugo.io) extended binary on it, for building hugo websites (usually in CI).

See `.gitlab-ci.yml` variable for latest version published. Images are tagged at [https://hub.docker.com/r/mosstech/hugo-build](https://hub.docker.com/r/mosstech/hugo-build).
