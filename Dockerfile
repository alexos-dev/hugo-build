FROM ubuntu:plucky-20241213

ARG HUGO_VERSION="0.142.0"

ENV HUGO_NAME="hugo_extended_withdeploy_${HUGO_VERSION}_Linux-64bit"
ENV HUGO_BASE_URL="https://github.com/gohugoio/hugo/releases/download"
ENV HUGO_DOWNLOAD_URL="${HUGO_BASE_URL}/v${HUGO_VERSION}/${HUGO_NAME}.tar.gz"
ENV HUGO_CHECKSUM_URL="${HUGO_BASE_URL}/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_checksums.txt"

# RUN apk add --no-cache --virtual .build-deps wget
# RUN apk add --no-cache git gettext bash make ca-certificates libc6-compat libstdc++
# # fix for go1.20 on alpine
# RUN apk add --no-cache \
#     gcompat && \
#     ln -s /lib/libc.so.6 /usr/lib/libresolv.so.2

RUN apt-get update && apt-get install -y wget

RUN wget --quiet ${HUGO_DOWNLOAD_URL} && \
wget --quiet "${HUGO_CHECKSUM_URL}" && \
grep "${HUGO_NAME}.tar.gz" "./hugo_${HUGO_VERSION}_checksums.txt" | sha256sum -c -

# RUN tar -zxvf "${HUGO_NAME}.tar.gz" && \
#     mv ./hugo /usr/local/bin/hugo && \
#     hugo version && \
#     apk del .build-deps && \
#     rm -rf /build

RUN tar -zxvf "${HUGO_NAME}.tar.gz" && \
    mv ./hugo /usr/local/bin/hugo && \
    hugo version && \
    rm -rf /build

CMD ["/bin/bash"]
